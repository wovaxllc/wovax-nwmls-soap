<?php
ini_set('memory_limit','4096M');
ini_set('display_errors', '1');
set_time_limit(0);
include_once('./request-manager.php');
include_once('./update-table-schema.php');
include_once('./update-listings.php');

$settings 	= SettingsManger::getInstance();
$request	= RequestManager::getInstance();
$prop_types	= $settings->get('properties_to_update', array(
	'BUSO',
	'RESI',
	'MANU',
	'FARM',
	'COMI',
	'VACL',
	'MULT',
	'COND'
));
foreach($prop_types as $type){
	$updater = new ListingUpdate($type);
	$updater->update();
}