<?php
include_once('./login-info.php');

Class SettingsManger{
	private static $instance = NULL;
	private $table = 'NWMLS_Settings';
	
	private function buildTable(){
		$db_conn = get_db_connection();
		$sql = 'CREATE TABLE IF NOT EXISTS `'.$this->table.'` (';
		$sql .= '`id` INT(10) NOT NULL PRIMARY KEY AUTO_INCREMENT,';
		$sql .= '`name` VARCHAR(255) NOT NULL UNIQUE KEY,';
		$sql .= '`value` TEXT NOT NULL,';
		$sql .= '`default_value` TEXT NOT NULL';
		$sql .= ') ENGINE=InnoDB DEFAULT CHARSET=utf8;';
		$results = $db_conn->query($sql);
		if($results === FALSE){
			die("Query failed: ".$db_conn->error);
		}
		return true;
	}
	//gets the one instance of this class.
	public static function getInstance() {
		if (is_null(self::$instance)) {
			self::$instance = new self;
			self::$instance->buildTable();
		}
		return self::$instance;
	}
	
	public function create($name, $value = '', $default = ''){
		$db_conn = get_db_connection();
		$sql = 'INSERT INTO `'.$this->table.'` (`name`, `value`, `default_value`) VALUES';
		$sql .= "('".$db_conn->real_escape_string($name)."',";
		$sql .= "'".serialize($value)."',";
		$sql .= "'".serialize($default)."');";
		$results = $db_conn->query($sql);
		if($results === FALSE){
			die("Query failed: ".$db_conn->error);
		}
		return true;
	}
	
	public function update($name, $value, $default = ''){
		$db_conn = get_db_connection();
		$sql = 'SELECT count(*) as `count` FROM `'.$this->table."` WHERE `name` = '".$db_conn->real_escape_string($name)."';";
		$results = $db_conn->query($sql);
		if($results === FALSE){
			die("Query failed: ".$db_conn->error);
		}
		if(intval($results->fetch_array(MYSQLI_ASSOC)['count']) < 1){
			return $this->create($name, $value, $default);
		}
		$sql = 'UPDATE `'.$this->table."` SET `value` = '".serialize($value)."'";
		if(!empty($default)){
			$sql .= ", `default_value` = '".serialize($default)."'";
		}
		$sql .= " WHERE `name` = '".$name."';";
		$results = $db_conn->query($sql);
		if($results === FALSE){
			die("Query failed: ".$db_conn->error);
		}
		return true;
	}
	
	public function get($name, $default = ''){
		$value = $this->onlyGet($name);
		if(empty($value) AND !empty($default)){
			$value = $default;
			$this->update($name, $default, $default);
		}
		return $value;
	}
	
	private function onlyGet($name){
		$db_conn = get_db_connection();
		$sql = 'SELECT `value`, `default_value` FROM `'.$this->table."` WHERE `name` = '".$db_conn->real_escape_string($name)."';";
		$results = $db_conn->query($sql);
		if($results === FALSE){
			die("Query failed: ".$db_conn->error);
		}
		$setting = $results->fetch_array(MYSQLI_ASSOC);
		if(empty($setting)){
			return '';
		}
		$value = unserialize($setting['value']);
		if(empty($value)){
			$def_value = unserialize($setting['default_value']);
			return $def_value;
		}
		return $value;
	}
}