<?php
include_once('./settings-manager.php');
include_once('./xmlstr_to_array.php');//Using this simple library since it seems to work well

Class RequestManager {
	private $prop_types = array();
	private $statuses	= array();
	private $user		= '';
	private $password	= '';
	private static $instance = NULL;
	protected function __construct(){
		$settings 			= SettingsManger::getInstance();
		$this->user			= $settings->get('NWMLS_Login', 'wovax');
		$this->password		= $settings->get('NWMLS_Password', 'yekE=Aze7u');
	}

	//gets the one instance of this class.
	public static function getInstance() {
		if (is_null(self::$instance)) {
			self::$instance = new self;
		}
		return self::$instance;
	}

	public function listingQuery($parameters, $type, $columns){
		$short_to_long = array(
			'BUSO' => 'BusinessOpportunity',
			'COMI' => 'CommercialIndustrial',
			'COND' => 'Condominium',
			'FARM' => 'FarmRanch',
			'MANU' => 'Manufactured',
			'MULT' => 'MultiFamily',
			'RENT' => 'Rental',
			'RESI' => 'Residential',
			'TSHR' => 'TimeShare',
			'VACL' => 'VacantLand'
		);
		if(!array_key_exists($type, $short_to_long)){
			return array();
		}
		$data = array();
		$data['Head'] = array(
			'UserId' => $this->user,
			'Password' => $this->password,
			'SchemaName' => 'StandardXML1_3'
		);
		$data['Body'] = array();
		$data['Body']['Query'] = array();
		$data['Body']['Filter'] = is_array($columns) ? implode(',',$columns) : $columns;
		$data['Body']['Query']['MLS'] = 'NWMLS';
		$data['Body']['Query']['PropertyType'] = $type;
		$data['Body']['Query'] = array_merge($data['Body']['Query'], $parameters);
		//done creating request array
		$xml = new DOMDocument("1.0", "utf-8");
		$xml->xmlStandalone = false;
		$spec = $xml->createElement('EverNetQuerySpecification');
		$spec->setAttribute('xmlns', 'urn:www.nwmls.com/Schemas/General/EverNetQueryXML.xsd');
		$message = $xml->createElement('Message');
		$this->build_xml($data, $xml, $message);
		$spec->appendChild($message);
		$xml->appendChild($spec);
		$XMLQuery = $xml->saveXML();
		$type = $short_to_long[$type];
		$client = new SoapClient('http://evernet.nwmls.com/evernetqueryservice/evernetquery.asmx?WSDL', array('trace' => 1));
		$params = array ("v_strXmlQuery" => $XMLQuery);//This case senseitive such a time waster!!!
		$data = $client->RetrieveListingData($params);
		$data = xmlstr_to_array($data->RetrieveListingDataResult);
		if(array_key_exists('ResponseMessages', $data)){
			throw new Exception(var_export($data, true));
		}
		if(!array_key_exists($type, $data)){
			return array();
		};
		$data = $data[$type];
		if(!array_key_exists(0, $data)){
			$data = array($data);
		}
		$new_data = array();
		foreach($data as $prop){
			$tmp_prop = array();
			foreach($prop as $key => $val){
				if(empty($val)){
					$val = '';
				}
				$tmp_prop[strtolower($key)] = $val;	
			}
			if(array_key_exists('ln', $tmp_prop) and !empty($tmp_prop['ln'])){
				$new_data[$tmp_prop['ln']] = $tmp_prop;
			} else {
				$new_data[] = $tmp_prop;
			}
		}
		unset($data);
		return $new_data;
	}

	public function getColumnTypes(){
		$data = array();
		$data['Head'] = array(
			'UserId' => $this->user,
			'Password' => $this->password,
			'SchemaName' => 'StandardXML1_3'
		);
		$data['Body'] = array();
		$data['Body']['Query'] = array();
		$data['Body']['Query']['MLS'] = 'NWMLS';
		$data['Body']['Filter'] = array();
		//done creating request array
		$xml = new DOMDocument("1.0", "utf-8");
		$xml->xmlStandalone = false;
		$spec = $xml->createElement('EverNetQuerySpecification');
		$spec->setAttribute('xmlns', 'urn:www.nwmls.com/Schemas/General/EverNetQueryXML.xsd');
		$message = $xml->createElement('Message');
		$this->build_xml($data, $xml, $message);
		$spec->appendChild($message);
		$xml->appendChild($spec);
		$XMLQuery = $xml->saveXML();
		$params = array ('v_strXMLQuery' => $XMLQuery);//This case senseitive such a time waster!!!	
		$client = new SoapClient('http://evernet.nwmls.com/evernetdiscoveryservice/evernetdiscovery.asmx?WSDL', array('trace' => 1));	
		$result = $client->RetrieveSchema($params);
		$data 	= xmlstr_to_array($result->RetrieveSchemaResult);
		$data	= $data['xs:element']['xs:complexType']['xs:choice']['xs:element'];
		$types	= array();
		foreach($data as $type){
			$info = array('name' => $type['@attributes']['name'], 'short-name' => $type['@attributes']['id'], 'columns' => array());
			$columns = $type['xs:complexType']['xs:all']['xs:element'];
			foreach($columns as $column){
				//echo $column['@attributes']['name'];
				$type = $column['@attributes']['type'];
				$name = strtolower($column['@attributes']['name']);
				if(preg_match('/^mstns:string([0-9]+$)/', $type)){
					$length = intval(substr($type, 12));
					$type = ($length <= 10) ? 'CHAR('.$length.')' : 'VARCHAR('.$length.')';
				} elseIf(preg_match('/^xs:nonNegativeInteger$/', $type)){
					$type = 'INT(11) UNSIGNED';	
				} elseIf(preg_match('/^mstns:dateTime$/', $type)){
					$type = 'DATETIME';	
				}elseIf (preg_match('/^xs:integer$/', $type)){
					$type = 'INT(11)';
				} elseIf (preg_match('/^xs:decimal$/', $type)){
					$type = 'VARCHAR(255)';
				} else {
					$type = 'TEXT';
				}
				$info['columns'][$name] = $type;
			}
			$main = array();
			if(array_key_exists('ln', $info['columns'])){
				$main['ln']	= $info['columns']['ln'];
				unset($info['columns']['ln']);
			}
			if(array_key_exists('st', $info['columns'])){
				$main['st']	= $info['columns']['st'];
				unset($info['columns']['st']);
			}
			if(array_key_exists('ptyp', $info['columns'])){
				$main['ptyp'] = $info['columns']['ptyp'];
				unset($info['columns']['ptyp']);
			}
			if(array_key_exists('ld', $info['columns'])){
				$main['ld']	= $info['columns']['ld'];
				unset($info['columns']['ld']);
			}
			if(array_key_exists('ud', $info['columns'])){
				$main['ud']	= $info['columns']['ud'];
				unset($info['columns']['ud']);
			}
			ksort($info['columns']);
			$info['columns'] = array_merge($main, $info['columns']);
			$types[] = $info;
		}
		return $types;
	}

	public function getPhotoInfo($listing_number){
		$parameters = array(
			'ListingNumber' => intval($listing_number)
		);
		$XMLQuery = $this->buildQuery($parameters);
		$client = new SoapClient('http://evernet.nwmls.com/evernetqueryservice/evernetquery.asmx?WSDL', array('trace' => 1));
		$params = array ("v_strXmlQuery" => $XMLQuery);//This case senseitive such a time waster!!!
		$data = $client->RetrieveImageData($params);
		$data = xmlstr_to_array($data->RetrieveImageDataResult);
		if(!array_key_exists('image', $data)){
			return array();
		}
		$data = $data['image'];
		if(array_key_exists('MLNumber', $data)){
			$data = array($data);
		}
		return $data;
	}


	public function photoQuery($parameters, $type){
		$XMLQuery = $this->buildQuery($parameters, $type);
		$client = new SoapClient('http://evernet.nwmls.com/evernetqueryservice/evernetquery.asmx?WSDL', array('trace' => 1));
		$params = array ("v_strXmlQuery" => $XMLQuery);//This case senseitive such a time waster!!!
		$data = $client->RetrieveImageData($params);
		$data = xmlstr_to_array($data->RetrieveImageDataResult);
		if(!array_key_exists('image', $data)){
			return array();
		}
		$data = $data['image'];
		if(array_key_exists('MLNumber', $data)){
			$data = array($data);
		}
		return $data;
	}

	private function buildQuery($parameters, $type = '', $columns = array()){
		$short_to_long = array(
			'BUSO' => 'BusinessOpportunity',
			'COMI' => 'CommercialIndustrial',
			'COND' => 'Condominium',
			'FARM' => 'FarmRanch',
			'MANU' => 'Manufactured',
			'MULT' => 'MultiFamily',
			'RENT' => 'Rental',
			'RESI' => 'Residential',
			'TSHR' => 'TimeShare',
			'VACL' => 'VacantLand'
		);
		if(!empty($type)){
			if(!array_key_exists($type, $short_to_long)){
				return '';
			}
		}
		$data = array();
		$data['Head'] = array(
			'UserId' => $this->user,
			'Password' => $this->password,
			'SchemaName' => 'StandardXML1_3'
		);
		$data['Body'] = array();
		$data['Body']['Query'] = array();
		$data['Body']['Filter'] = is_array($columns) ? implode(',',$columns) : $columns;
		$data['Body']['Query']['MLS'] = 'NWMLS';
		if(!empty($type)){
			$data['Body']['Query']['PropertyType'] = $type;
		}
		$data['Body']['Query'] = array_merge($data['Body']['Query'], $parameters);
		//done creating request array
		$xml = new DOMDocument("1.0", "utf-8");
		$xml->xmlStandalone = false;
		$spec = $xml->createElement('EverNetQuerySpecification');
		$spec->setAttribute('xmlns', 'urn:www.nwmls.com/Schemas/General/EverNetQueryXML.xsd');
		$message = $xml->createElement('Message');
		$this->build_xml($data, $xml, $message);
		$spec->appendChild($message);
		$xml->appendChild($spec);
		return $xml->saveXML();
	}

	private function build_xml($array, $doc, $dom){
		foreach($array as $key => $value){
			$key = strval($key);
			if(is_array($value)){
				$section = $doc->createElement($key);
				$this->build_xml($value, $doc, $section);
				$dom->appendChild($section);
			} else {
				$value = strval($value);
				$section = $doc->createElement($key, $value);
				$dom->appendChild($section);
			}
		}
		return $dom;
	}
}