<?php
ini_set('memory_limit','256M');
set_time_limit(0);
ignore_user_abort(true);


include_once('./login-info.php');
include_once('./settings-manager.php');
include_once('./xmlstr_to_array.php');//Using this simple library since it seems to work well

function get_schools($login, $password){
	$client = new SoapClient('http://evernet.nwmls.com/evernetqueryservice/evernetquery.asmx?WSDL', array('trace' => 1));
	$XMLQuery = "<?xml version='1.0' encoding='utf-8' standalone='no' ?>";
    $XMLQuery .= "<EverNetQuerySpecification  xmlns='urn:www.nwmls.com/Schemas/General/EverNetQueryXML.xsd'>";
    $XMLQuery .= "<Message>";
    $XMLQuery .= "<Head>";
    $XMLQuery .= "<UserId>".$login."</UserId>";
    $XMLQuery .= "<Password>".$password."</Password>";
    $XMLQuery .= "<SchemaName>StandardXML1_3</SchemaName>";
    $XMLQuery .= "</Head>";
    $XMLQuery .= "<Body>";
    $XMLQuery .= "<Query>";
    $XMLQuery .= "<MLS>NWMLS</MLS><ListingNumber></ListingNumber>";
    $XMLQuery .= "</Query>";
    $XMLQuery .= "<Filter></Filter>";
    $XMLQuery .= "</Body>";
    $XMLQuery .= "</Message>";
    $XMLQuery .= "</EverNetQuerySpecification >";
	$params = array ("v_strXmlQuery" => $XMLQuery);//This case senseitive such a time waster!!!
	$data = $client->RetrieveSchoolData($params);
	$data = xmlstr_to_array($data->RetrieveSchoolDataResult)['school'];
	unset($data[0]);
	return $data;
}

/*
function xmlObj2Array($xmlObject, $out = array ())
{
        foreach ((array) $xmlObject as $index => $node){
            $out[$index] = (is_object($node) ||  is_array($node)) ? xmlObj2Array($node) : $node;
		}
		if(empty($out)){
			return '';
		}
        return $out;
}*/

function convert($size){
    $unit=array('b','kb','mb','gb','tb','pb');
    return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
}

echo "<h3>Starting School Update Proccess</h3>\n";
$settings = SettingsManger::getInstance();
$user			= $settings->get('NWMLS_Login', 'wovax');
$pwd			= $settings->get('NWMLS_Password', 'yekE=Aze7u');
$schools = get_schools($user, $pwd);
echo "School Count: ".count($schools)."<br>\n";
echo "Memory: ".convert(memory_get_usage(true))."<br>\n";

$sql = 'CREATE TABLE IF NOT EXISTS `School_Dis` (';
$sql .= '`id` INT(10) NOT NULL PRIMARY KEY AUTO_INCREMENT,';
$sql .= '`district_code` varchar(32) NOT NULL,';
$sql .= '`district_name` varchar(128) NOT NULL,';
$sql .= 'UNIQUE KEY `district_key` (`district_code`)';
$sql .= ') ENGINE=InnoDB DEFAULT CHARSET=utf8;';
$results = get_db_connection()->query($sql);
if($results === FALSE){
	die("Query failed: ".get_db_connection()->error);
}

foreach($schools as $school){
	update_school($school['SchoolDistrictCode'], $school['SchoolDistrictDescription']);
}

function update_school($district_code, $district){
	$conn = get_db_connection();
	$sql = "SELECT * FROM `School_Dis` WHERE `district_code` = '".$district_code."';";
	$ret = $conn->query($sql);
	if($tmp = $ret->fetch_array(MYSQLI_ASSOC)){
		$sql = "UPDATE `School_Dis` SET `district_name` = '".$district."' WHERE `district_code` = '".$district_code."';";
	} else {
		$sql =  "INSERT INTO `School_Dis` (`district_code`, `district_name`) VALUES ";
		$sql .= "('".$district_code."', '".$district."');";
	}
	echo $sql."<br>\n";
	return $conn->query($sql);
}