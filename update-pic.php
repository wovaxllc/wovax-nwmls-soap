<?php
include_once('./login-info.php');
include_once('./status-manager.php');
include_once('./settings-manager.php');
include_once('./xmlstr_to_array.php');//Using this simple library since it seems to work well

set_time_limit(0);
ignore_user_abort(true);

echo "<h3>Starting Photo Update Proccess</h3>\n";
$settings	= SettingsManger::getInstance();
$status		= StatusManager::getInstance();
$user		= $settings->get('NWMLS_Login', 'wovax');
$pass		= $settings->get('NWMLS_Password', 'yekE=Aze7u');
$photo_key  = $settings->get('photo_key', 'W5Jg2aHAZ28zscZX4Xzcxh');
$base 		= $settings->get('base_url', 'http://mls.wovax.io/wovax-nwmls/pic.php');
$types		= $settings->get('properties_to_update');
$list_nums	= $status->getListingsByPhotoStatus(0);
$dbConn		= get_db_connection();
$time_start	= time();

$sql = 'CREATE TABLE IF NOT EXISTS `NWMLS_Photos` (';
$sql .= '`id` INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,';
$sql .= '`Image` varchar(128) NOT NULL,';
$sql .= '`MLS_ID` INT(10) NOT NULL,';
$sql .= '`description` varchar(256) NOT NULL,';
$sql .= 'UNIQUE KEY `image_key` (`Image`)';
$sql .= ') ENGINE=InnoDB DEFAULT CHARSET=utf8;';
$results = $dbConn->query($sql);
if($results === FALSE){
	die("Query failed: ".$dbConn->error);
}
//echo "Propeties without images: ".count($list_nums)."<br>\n";
$list_nums = array_intersect(get_unsold_listings($dbConn, $types), $list_nums); ;
echo "Unsold propeties without images: ".count($list_nums)."<br>\n";
//get unchecked propeties
$list_nums = array_slice($list_nums, 0, 100);
$insert_parts = array();
$statuses = array();
foreach($list_nums as $ln){
	$images = get_photo_info($user, $pass, $ln);
	if(empty($images)){
		$statuses[] = array('ln' => $ln, 'status' => 2);
	}
	foreach($images as $index => $image){
		$image = mysqli_escape_array($dbConn, $image);
		$url = build_url($base, $image["PictureFileName"], $photo_key);
		$insert_parts[] = "(".$image["MLNumber"].", '".$url."', '".$image["PictureDescription"]."')";
	}
	$statuses[] = array('ln' => $ln, 'status' => 1);
}
if(!empty($insert_parts)){
	$sql = "INSERT INTO `NWMLS_Photos` (`MLS_ID`, `Image`, `description`) VALUES ";
	$sql .= implode(", ", $insert_parts).";";
	$results = $dbConn->query($sql);
	if($results === FALSE){
		die("Query failed: ".$dbConn->error);
	}
	$status->updatePhotoStatusMulti($statuses);
}
echo "Images Inserted: ".count($insert_parts)."<br>\n";


//next find any with image count mismatch
$needs_updated = array();
foreach($types as $type){
	$sql = "SELECT COUNT(*) AS `count`, `ln`, `pic` AS `count_needed` FROM `NWMLS_Photos` ";
	$sql .= "LEFT JOIN `".$type."_Data` ON `ln` = `MLS_ID` GROUP BY `MLS_ID` HAVING COUNT(*) != `pic`;";
	echo $sql."<br>\n";
	$results = $dbConn->query($sql);
	if($results === FALSE){
		die("Query failed: ".$dbConn->error);
	}
	while($data = $results->fetch_array(MYSQLI_ASSOC)){
		$needs_updated[] = $data;
	}
}
echo "Listings with image mismatch: <b>".count($needs_updated)."</b><br>\n"; 
foreach($needs_updated as $listing){
	$images = get_photo_info($user, $pass, $listing['ln']);
	$stat = count($images) === $listing['count_needed'] ? 1 : 3;
	$old_urls = get_urls($dbConn, $listing['ln']);
	$urls = array();
	foreach($images as $index => $image){
		$image = mysqli_escape_array($dbConn, $image);
		$url = build_url($base, $image["PictureFileName"], $photo_key);
		$urls[] = $url;
		$sql = "INSERT IGNORE INTO `NWMLS_Photos` (`MLS_ID`, `Image`, `description`) ";
		$sql .= "VALUES (".$image["MLNumber"].", '".$url."', '".$image["PictureDescription"]."')";
		$results = $dbConn->query($sql);
		if($results === FALSE){
			echo "Query failed: ".$dbConn->error."<br>\n";
		}
	}
	$bad_urls = array_diff($old_urls, $urls);
	foreach($bad_urls as $removed_url){
		echo "Removing URL: ".$removed_url."<br>\n";
		delete_url($dbConn, $removed_url);
	}
	$status->updatePhotoStatus($listing['ln'], $stat);
	echo "Updated: <b>".$listing['ln']."</b>  (<b>".count($images)."</b> of <b>".$listing['count_needed']."</b>)<br>\n";
}

echo "Done in <b>".(time()-$time_start)."</b> seconds<br>\n";
refresh();
close_db();

//FUNCTIONS
function get_unsold_listings($db_conn, $types){
	$lns = array();
	foreach($types as $type){
		$sql = "SELECT `ln` FROM `".$type."_Data` WHERE `st` != 'S'";
		$results = $db_conn->query($sql);
		if($results === FALSE){
			die("Query failed: ".$db_conn->error);
		}
		while($ln = $results->fetch_array(MYSQLI_ASSOC)){
			$lns[] = intval($ln['ln']);
		}
	}
	return $lns;
}

function get_photo_info($user, $pass, $ln){
	$client = new SoapClient('http://evernet.nwmls.com/evernetqueryservice/evernetquery.asmx?WSDL', array('trace' => 1));
	$XMLQuery = "<?xml version='1.0' encoding='utf-8' standalone='no' ?>";
    $XMLQuery .= "<EverNetQuerySpecification  xmlns='urn:www.nwmls.com/Schemas/General/EverNetQueryXML.xsd'>";
    $XMLQuery .= "<Message>";
    $XMLQuery .= "<Head>";
    $XMLQuery .= "<UserId>".$user."</UserId>";
    $XMLQuery .= "<Password>".$pass."</Password>";
    $XMLQuery .= "<SchemaName>StandardXML1_3</SchemaName>";
    $XMLQuery .= "</Head>";
    $XMLQuery .= "<Body>";
    $XMLQuery .= "<Query>";
    $XMLQuery .= "<MLS>NWMLS</MLS><ListingNumber>".$ln."</ListingNumber>";
    $XMLQuery .= "</Query>";
    $XMLQuery .= "<Filter></Filter>";
    $XMLQuery .= "</Body>";
    $XMLQuery .= "</Message>";
    $XMLQuery .= "</EverNetQuerySpecification >";
	$params = array ("v_strXmlQuery" => $XMLQuery);//This case senseitive such a time waster!!!
	$data = $client->RetrieveImageData($params);
	$data = xmlstr_to_array($data->RetrieveImageDataResult);
	if(!array_key_exists('image', $data)){
		return array();
	}
	$data = $data['image'];
	if(array_key_exists('MLNumber', $data)){
		$data = array($data);
	}
	return $data;
}

function build_url($base, $file, $key){
	$filler = 'nwmls_imgs';
	$hash =  hash_hmac('sha1', $filler.'|'.$file, $key);
	return $base."/".$filler."/".$hash."/".$file;
}

function refresh(){
	$refresh = array_key_exists('refresh', $_REQUEST) ? $_REQUEST['refresh'] : '0';
	if($refresh !== '0'){
		echo '<meta http-equiv="refresh" content="1;url='.basename(__FILE__, '').'?refresh=1">';
	}
}

function delete_url($db_conn, $url){
	$sql = "DELETE FROM `NWMLS_Photos` WHERE `Image` = '".$url."';";
	$results = $db_conn->query($sql);
	if($results === FALSE){
		echo "Query failed: ".$db_conn->error."<br>\n";
	}
}

function get_urls($db_conn, $ln){
	$sql = "SELECT `Image` FROM `NWMLS_Photos` WHERE `MLS_ID` = ".$ln.";";
	$results = $db_conn->query($sql);
	if($results === FALSE){
		echo "Query failed: ".$db_conn->error."<br>\n";
	}
	$urls = array();
	while($url = $results->fetch_array(MYSQLI_ASSOC)){
		$urls[] = $url['Image'];
	}
	return $urls;
}