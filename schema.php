<?php
include_once('./xmlstr_to_array.php');//Using this simple library since it seems to work well

function get_sql_types($login, $password){
	$client=new SoapClient('http://evernet.nwmls.com/evernetdiscoveryservice/evernetdiscovery.asmx?WSDL', array('trace' => 1));	
    $XMLQuery ="<?xml version='1.0' encoding='utf-8' standalone='no' ?>";
    $XMLQuery .="<EverNetQuerySpecification  xmlns='urn:www.nwmls.com/Schemas/General/EverNetQueryXML.xsd'>";
    $XMLQuery .="<Message>";
    $XMLQuery .="<Head>";
    $XMLQuery .="<UserId>".$login."</UserId>";
    $XMLQuery .="<Password>".$password."</Password>";
    $XMLQuery .="<SchemaName>StandardXML1_3</SchemaName>";
    $XMLQuery .="</Head>";
    $XMLQuery .="<Body>";
    $XMLQuery .="<Query>";
    $XMLQuery .="<MLS>NWMLS</MLS>";
    $XMLQuery .="</Query>";
    $XMLQuery .="<Filter></Filter>";
    $XMLQuery .="</Body>";
    $XMLQuery .="</Message>";
    $XMLQuery .="</EverNetQuerySpecification >";
	$params = array ('v_strXMLQuery' => $XMLQuery);//This case senseitive such a time waster!!!	
	$result = $client->RetrieveSchema($params);
	$data 	= xmlstr_to_array(utf8_encode($result->RetrieveSchemaResult));
	$data	= $data['xs:element']['xs:complexType']['xs:choice']['xs:element'];
	$types	= array();
	foreach($data as $type){
		$info = array('name' => $type['@attributes']['name'], 'short-name' => $type['@attributes']['id'], 'columns' => array());
		$columns = $type['xs:complexType']['xs:all']['xs:element'];
		foreach($columns as $column){
			//echo $column['@attributes']['name'];
			$type = $column['@attributes']['type'];
			$name = strtolower($column['@attributes']['name']);
			if(preg_match('/^mstns:string([0-9]+$)/', $type)){
				$length = intval(substr($type, 12));
				$type = ($length <= 10) ? 'CHAR('.$length.')' : 'VARCHAR('.$length.')';
			} elseIf(preg_match('/^xs:nonNegativeInteger$/', $type)){
				$type = 'INT(11) UNSIGNED';	
			} elseIf(preg_match('/^mstns:dateTime$/', $type)){
				$type = 'DATETIME';	
			}elseIf (preg_match('/^xs:integer$/', $type)){
				$type = 'INT(11)';
			} elseIf (preg_match('/^xs:decimal$/', $type)){
				$type = 'VARCHAR(255)';
			} else {
				$type = 'TEXT';
			}
			$info['columns'][$name] = $type;
		}
		$main = array();
		if(array_key_exists('ln', $info['columns'])){
			$main['ln']	= $info['columns']['ln'];
			unset($info['columns']['ln']);
		}
		if(array_key_exists('st', $info['columns'])){
			$main['st']	= $info['columns']['st'];
			unset($info['columns']['st']);
		}
		if(array_key_exists('ptyp', $info['columns'])){
			$main['ptyp'] = $info['columns']['ptyp'];
			unset($info['columns']['ptyp']);
		}
		if(array_key_exists('ld', $info['columns'])){
			$main['ld']	= $info['columns']['ld'];
			unset($info['columns']['ld']);
		}
		if(array_key_exists('ud', $info['columns'])){
			$main['ud']	= $info['columns']['ud'];
			unset($info['columns']['ud']);
		}
		ksort($info['columns']);
		$info['columns'] = array_merge($main, $info['columns']);
		$types[] = $info;
	}
	return $types;
}
?>