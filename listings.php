<?php
include_once('./xmlstr_to_array.php');//Using this simple library since it seems to work well

function get_listings_by_date($login, $password, $type, $begin_date, $end_date){
	$client = new SoapClient('http://evernet.nwmls.com/evernetqueryservice/evernetquery.asmx?WSDL', array('trace' => 1));
	$XMLQuery = "<?xml version='1.0' encoding='utf-8' standalone='no' ?>";
    $XMLQuery .= "<EverNetQuerySpecification  xmlns='urn:www.nwmls.com/Schemas/General/EverNetQueryXML.xsd'>";
    $XMLQuery .= "<Message>";
    $XMLQuery .= "<Head>";
    $XMLQuery .= "<UserId>".$login."</UserId>";
    $XMLQuery .= "<Password>".$password."</Password>";
    $XMLQuery .= "<SchemaName>StandardXML1_3</SchemaName>";
    $XMLQuery .= "</Head>";
    $XMLQuery .= "<Body>";
    $XMLQuery .= "<Query>";
    $XMLQuery .= "<MLS>NWMLS</MLS>";
	$XMLQuery .= "<PropertyType>".$type."</PropertyType>";
	$XMLQuery .= "<BeginDate>".$begin_date."</BeginDate>";
	$XMLQuery .= "<EndDate>".$end_date."</EndDate>";
    $XMLQuery .= "</Query>";
    $XMLQuery .= "<Filter></Filter>";
    $XMLQuery .= "</Body>";
    $XMLQuery .= "</Message>";
    $XMLQuery .= "</EverNetQuerySpecification >";
	$params = array ("v_strXmlQuery" => $XMLQuery);//This case senseitive such a time waster!!!
	$data = $client->RetrieveListingData($params);
	$data = xmlstr_to_array($data->RetrieveListingDataResult);
	$data = reset($data);
	if(is_string($data)){
		return array();
	}
	if(array_key_exists('LN', $data)){
		$data = array($data);
	}
	$new_data = array();
	foreach($data as $prop){
		$tmp_prop = array();
		foreach($prop as $key => $val){
			if(empty($val)){
				$val = '';
			}
			$tmp_prop[strtolower($key)] = $val;	
		}
		$new_data[] = $tmp_prop;
	}
	unset($data);
	return $new_data;
}


function get_listing_data($username, $pass, $start_date, $end_date, $type = ''){
	$XMLQuery  = '<?xml version="1.0" encoding="utf-8" standalone="no"?>';
	$XMLQuery .= '<EverNetQuerySpecification xmlns="urn:www.nwmls.com/Schemas/General/EverNetQueryXML.xsd">';
	$XMLQuery .= '<Message>';  
	$XMLQuery .= '<Head><UserId>'.$username.'</UserId><Password>'.$pass.'</Password><SchemaName>StandardXML1_3</SchemaName></Head>';
	$XMLQuery .= '<Body>';
	$XMLQuery .= '<Query><MLS>NWMLS</MLS><ListingNumber></ListingNumber>';
	if(!empty($type)){
		$XMLQuery .= '<PropertyType>'.$type.'</PropertyType>';
	}
	$XMLQuery .= '<BeginDate>'.$start_date.'</BeginDate>';
	$XMLQuery .= '<EndDate>'.$end_date.'</EndDate>';
	$XMLQuery .= '</Query>';
	$XMLQuery .= '<Filter/></Body>';
	$XMLQuery .= '</Message>';
	$XMLQuery .= '</EverNetQuerySpecification>';
	$XMLQuery = array("v_strXmlQuery" => $XMLQuery);
	$qtype = "RetrieveListingData";
	$wsdl = "http://evernet.nwmls.com/evernetqueryservice/evernetquery.asmx?WSDL";
	$client = new nusoap_client($wsdl,true);
	$err = $client->getError();
	if($err){
		echo '<h2>Constructor error</h2>'.$err;// Display the error
		exit();
	}
	$proxy = $client->getProxy();
	$response = $proxy->$qtype($XMLQuery);
	$client = new nusoap_client($wsdl, 'wsdl');
	$array = xmlObj2Array(simplexml_load_string($response[$qtype."Result"]));
	if(!is_array($array)){
		return array();
	}
	$array = reset($array);
	if(array_key_exists('LN', $array)){
		$array = array($array);
	}
	return $array;
}