<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
$ln   = array_key_exists('ln', $_GET) ? intval($_GET['ln']) : 0;
$utf8 = array_key_exists('utf8', $_GET) ? (preg_match('/^(yes|true|1)$/', $_GET['utf8']) === 1) : FALSE;
$raw  = array_key_exists('raw', $_GET) ? (preg_match('/^(yes|true|1)$/', $_GET['raw']) === 1) : FALSE;
if($ln < 1) {
	exit('Invalid Listing Number!');
}

function build_xml($array, $doc, $dom){
	foreach($array as $key => $value){
		$key = strval($key);
		if(is_array($value)){
			$section = $doc->createElement($key);
			build_xml($value, $doc, $section);
			$dom->appendChild($section);
		} else {
			$value = strval($value);
			$section = $doc->createElement($key, $value);
			$dom->appendChild($section);
		}
	}
	return $dom;
}
function listingQuery($user, $password, $parameters, $type = '', $columns = array()){
	$short_to_long = array(
		'BUSO' => 'BusinessOpportunity',
		'COMI' => 'CommercialIndustrial',
		'COND' => 'Condominium',
		'FARM' => 'FarmRanch',
		'MANU' => 'Manufactured',
		'MULT' => 'MultiFamily',
		'RENT' => 'Rental',
		'RESI' => 'Residential',
		'TSHR' => 'TimeShare',
		'VACL' => 'VacantLand'
	);
	if(array_key_exists($type, $short_to_long) && strlen($type) > 0){
		return array();
	}
	$data = array();
	$data['Head'] = array(
		'UserId' => $user,
		'Password' => $password,
		'SchemaName' => 'StandardXML1_3'
	);
	$data['Body'] = array();
	$data['Body']['Query'] = array();
	$data['Body']['Filter'] = is_array($columns) ? implode(',',$columns) : $columns;
	$data['Body']['Query']['MLS'] = 'NWMLS';
	$data['Body']['Query'] = array_merge($data['Body']['Query'], $parameters);
	if(strlen($type) > 0) {
		$data['Body']['Query']['PropertyType'] = $type;
	}
	//done creating request array
	$xml = new DOMDocument("1.0", "utf-8");
	$xml->xmlStandalone = false;
	$spec = $xml->createElement('EverNetQuerySpecification');
	$spec->setAttribute('xmlns', 'urn:www.nwmls.com/Schemas/General/EverNetQueryXML.xsd');
	$message = $xml->createElement('Message');
	build_xml($data, $xml, $message);
	$spec->appendChild($message);
	$xml->appendChild($spec);
	$XMLQuery = $xml->saveXML();
	//$type = $short_to_long[$type];
	$client = new SoapClient('http://evernet.nwmls.com/evernetqueryservice/evernetquery.asmx?WSDL', array('trace' => 1));
	$params = array ("v_strXmlQuery" => $XMLQuery);//This case senseitive such a time waster!!!
	$data = $client->RetrieveListingData($params);
	return $data->RetrieveListingDataResult;
}
if($raw) {
	header('Content-type: application/octet-stream');
} else {
	header("Content-type: text/xml");
}
$data = listingQuery('wovax', 'yekE=Aze7u', array('ListingNumber' => $ln));
if($utf8) {
	$data = utf8_encode($data); 
}
echo $data;