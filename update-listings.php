<?php
include_once('./login-info.php');
include_once('./request-manager.php');
include_once('./settings-manager.php');

Class ListingUpdate {
	private $db_conn		= NULL;
	private $tmp_suffix		= '';
	private $start_time 	= 0;
	private $step_size		= 864000;
	private $pic_step_size	= 172800;
	private $max_run		= 3600;
	private $active_stats	= array('A', 'CT');
	private $type			= "";
	private $tmp_date_tbl	= "";
	private $tmp_pic_tbl	= "";
	private $data_tbl		= "";
	private $pic_key		= 'W5Jg2aHAZ28zscZX4Xzcxh';
	private $pic_base		= "";
	private static $run_start = 0;
	public function __construct($type){
		$settings			= SettingsManger::getInstance();
		$this->db_conn		= get_db_connection();
		$this->type			= $this->db_conn->escape_string($type);
		$this->max_run		= $settings->get('max_run_time', $this->max_run);
		$this->active_stats	= $settings->get('active_statuses', $this->active_stats);
		$this->start_time	= $settings->get('start_time', strtotime('January 1 2014'));
		$this->step_size	= $settings->get('step_size', $this->step_size);
		$this->pic_step_size= $settings->get('pic_step_size', $this->pic_step_size);
		$this->pic_key  	= $settings->get('pic_key', $this->pic_key);
		$this->pic_base 	= $settings->get('base_url', 'http://mls.wovax.io/wovax-nwmls/pic.php');
		$rand				= mt_rand();
		$this->tmp_date_tbl	= "Temp_".$this->type."_Dates_".$rand;
		$this->tmp_pic_tbl	= "Temp_".$this->type."_Pic_".$rand;
		$this->data_tbl		= $this->type."_Data";
		$this->createTempTables();
	}

	private function createTempTables(){
		$sql = 
		"CREATE TEMPORARY TABLE `%s` (
			`list_num` INT(10) UNSIGNED NOT NULL PRIMARY KEY,
			`status` char(5) NOT NULL,
			`update_date` datetime NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;";
		$sql = sprintf($sql, $this->tmp_date_tbl);
		$results = $this->db_conn->query($sql);
		if($results === FALSE){
			die("Query failed: ".$this->db_conn->error);
		}
		$sql = 
		"CREATE TABLE `%s` (
			`id` INT(10) UNSIGNED NOT NULL PRIMARY KEY,
			`MLS_ID` INT(10) UNSIGNED NOT NULL,
			`update_date` datetime NOT NULL,
			`Image` VARCHAR(128) NOT NULL,
			`description` VARCHAR(256) NOT NULL,
			 INDEX (`MLS_ID`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;";
		$sql = sprintf($sql, $this->tmp_pic_tbl);
		//$results = $this->db_conn->query($sql);
		//if($results === FALSE){
		//	die("Query failed: ".$this->db_conn->error);
		//}		
	}
	public function update(){
		$this->updateSchema();
		$this->fetchUpdateInfo();
		$this->markDeleted();
		$this->insertNew();
		$this->updateExisting();
	}

	public function updateSchema(){
		$request	= RequestManager::getInstance();
		$types		= $request->getColumnTypes();
		foreach($types as $type){
			if($type["short-name"] !== $this->type){
				continue;
			}
			$sql = 'CREATE TABLE IF NOT EXISTS `'.$this->data_tbl.'` (';
			$sql .= '`id` INT(10) NOT NULL PRIMARY KEY AUTO_INCREMENT';
			$sql .= ') ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;';
			$results = $this->db_conn->query($sql);
			if($results === FALSE){
				die("Query failed: ".$this->db_conn->error);
			}
			//begin column update process.
			$columns = $this->getDataTblColumns();
			$prev = '';
			foreach($type["columns"] AS $column => $col_type){
				if(in_array($column, $columns)){
					$prev = $column;
					continue;
				}
				$sql = 'ALTER TABLE `'.$this->data_tbl.'` ADD COLUMN `'.$column.'` '.$col_type.' NOT NULL';
				if($column === 'ln'){
					$sql .= ' UNIQUE KEY';
				}
				if(!empty($prev)){
					$sql .= ' AFTER `'.$prev.'`';
				}
				$sql .= ';';
				$results = $this->db_conn->query($sql);
				if($results === FALSE){
					echo $col_type.' ';
					echo $sql."\n";
					die("Query failed: ".$this->db_conn->error);
				}
				echo 'Added Column: '.$this->data_tbl.'.<b>'.$column."</b><br>\n";
				$prev = $column;
			}
		}
		/*$sql = "CREATE TABLE IF NOT EXISTS `%s` (
			`id` INT(10) UNSIGNED NOT NULL PRIMARY KEY,
			`MLS_ID` INT(10) UNSIGNED NOT NULL,
			`ptyp` CHAR(4) NOT NULL,
			`update_date` datetime NOT NULL,
			`Image` VARCHAR(128) NOT NULL,
			`description` VARCHAR(256) NOT NULL,
			 INDEX (`MLS_ID`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
		$sql = sprintf($sql, 'NWMLS_Pictures');
		$results = $this->db_conn->query($sql);
		if($results === FALSE){
			die("Query failed: ".$this->db_conn->error);
		}*/
		echo "Successfully updated all ".$this->data_tbl." schema.<br>\n";
	}

	/* This function inserts all the update dates since the oldest active property
	** or the start date which is ever newer into a tempory table. This let's us
	** make comparisons to find out what to update and insert.
	*/
	public function fetchUpdateInfo(){
		$step_start = $this->getStartTime();
		$end_time	= time();
		$request	= RequestManager::getInstance();
		$columns	= array('LN','ST','UD');
		$listings	= array();
		echo "Fetching <strong>".$this->type."</strong> Update Dates from ".date("Y-m-d H:i:s",($step_start-60*60))." to ".date("Y-m-d H:i:s", $end_time)."<br>\n";
		echo "Please Wait: ";
		while($step_start <= $end_time){
			$this->checkRunTime();
			echo ".";
			$step_end = $step_start+$this->step_size;
			$parameters = array(
				//subtract 1 hour to give us a good buffer to make sure
				//we don't miss anything
				'BeginDate'	=> date("Y-m-d\TH:i:s", ($step_start-60*60)),
				'EndDate'	=> date("Y-m-d\TH:i:s", $step_end)
			);
			$step_start = $step_end;
			$data = $request->listingQuery($parameters, $this->type, $columns);
			$rows = array();
			foreach($data as $listing){
				$ln = $this->db_conn->escape_string($listing['ln']);
				$st = $this->db_conn->escape_string($listing['st']);
				$ud = $this->db_conn->escape_string($listing['ud']);
				$sql_row = "('%s', '%s', '%s')";
				$sql_row = sprintf($sql_row, $ln, $st, $ud);
				$rows[] = $sql_row;
			}
			if(empty($rows)){
				continue;
			}
			$sql = "INSERT IGNORE INTO `%s` (`list_num`, `status`, `update_date`) VALUES\n%s";
			$sql = sprintf($sql, $this->tmp_date_tbl, implode(",\n", $rows));
			$results = $this->db_conn->query($sql);
			if($results === FALSE){
				throw new Exception("Query failed: ".$this->db_conn->error);
			}
		}
		echo "<br>\n";
		echo "Done Fetching <strong>".$this->type."</strong> Update Dates ".date("Y-m-d H:i:s",($step_start-60*60))."<br>\n";
		return TRUE;
	}

	public function fetchPicUpdateInfo(){
		$step_start = $this->getStartTime();
		$end_time	= time();
		$request	= RequestManager::getInstance();
		$listings	= array();
		echo "Fetching <strong>".$this->type."</strong> Photo Data From ".date("Y-m-d H:i:s",($step_start-60*60))." <strong>To</strong> ".date("Y-m-d H:i:s", $end_time)."<br>\n";
		echo "Please Wait: ";
		while($step_start <= $end_time){
			$this->checkRunTime();
			echo ".";
			$step_end = $step_start+$this->pic_step_size;
			$parameters = array(
				'BeginDate'	=> date("Y-m-d\TH:i:s", ($step_start-60*60)),
				'EndDate'	=> date("Y-m-d\TH:i:s", $step_end)
			);
			$step_start = $step_end;
			$data = $request->photoQuery($parameters, $this->type);
			$rows = array();
			foreach($data as $photo){
				$listing_num	= intval($photo["MLNumber"]);
				$file			= $this->db_conn->escape_string($photo["PictureFileName"]);
				$description	= empty($photo["PictureDescription"]) ? "" : $photo["PictureDescription"];
				$description	= $this->db_conn->escape_string($description);
				$modify_time	= $this->db_conn->escape_string($photo["LastModifiedDateTime"]);
				$image_num		= explode('_' , explode('.', $file)[0]);
				$image_num 		= (count($image_num) < 2) ? 0 : intval($image_num[1]);
				$id				= ($listing_num*100)+$image_num;
				$filler 		= 'nwmls_imgs|145143_06.jpg';

				$hash 			= hash_hmac('sha1', $filler.'|'.$file, $this->pic_key);
				$image_url		= $this->pic_base."/".$filler."/".$hash."/".$file;
				$sql_row = "('%d', '%d', '%s', '%s', '%s')";
				$sql_row = sprintf(
					$sql_row,
					$id,
					$listing_num,
					$modify_time,
					$image_url,
					$description
				);
				$rows[] = $sql_row;
			}
			echo " ".count($rows)." ";
			if(empty($rows)){
				continue;
			}
			$sql = "INSERT IGNORE INTO `%s` (`id`, `MLS_ID`, `update_date`, `Image`, `description`) VALUES\n%s";
			$sql = sprintf($sql, $this->tmp_pic_tbl, implode(",\n", $rows));
			$results = $this->db_conn->query($sql);
			if($results === FALSE){
				throw new Exception("Query failed: ".$this->db_conn->error);
			}
		}
	}
	public function getOldestActiveDate(){
		$sql		= "SELECT `ud` FROM `%s` WHERE `st` IN ('%s') ORDER BY `ud` ASC LIMIT 1;";
		$sql 		= sprintf($sql, $this->data_tbl, implode("', '", $this->active_stats));
		$results 	= $this->db_conn->query($sql);
		if($results === FALSE){
			throw new Exception("Query failed: ".$db_conn->error);
		}
		while($values = $results->fetch_array(MYSQLI_ASSOC)){
			return $values['ud'];
		}
		return FALSE;
	}

	public function getNewListings(){
		$sql = "SELECT `list_num`, `update_date` FROM `%s` LEFT JOIN `%s` on `ln` = `list_num` WHERE `ln` IS NULL ORDER BY `update_date` ASC;";
		$sql = sprintf($sql, $this->tmp_date_tbl, $this->data_tbl);
		$results = $this->db_conn->query($sql);
		if($results === FALSE){
			throw new Exception("Query failed: ".$db_conn->error);
		}
		$new_lns = array();
		while($values = $results->fetch_array(MYSQLI_ASSOC)){
			$new_lns[] = $values;
		}
		return $new_lns;
	}

	public function getUpdatedListings(){
		$sql = "SELECT `list_num`, `update_date` FROM `%s` LEFT JOIN `%s` on `ln` = `list_num` WHERE `ud` != `update_date` ORDER BY `update_date` ASC;";
		$sql = sprintf($sql, $this->tmp_date_tbl, $this->data_tbl);
		$results = $this->db_conn->query($sql);
		if($results === FALSE){
			throw new Exception("Query failed: ".$db_conn->error);
		}
		$new_lns = array();
		while($values = $results->fetch_array(MYSQLI_ASSOC)){
			$new_lns[] = $values;
		}
		return $new_lns;
	}

	public function getListing($ln){
		$request	= RequestManager::getInstance();
		$listings	= array();
		$parameters = array(
				'ListingNumber'	=> $ln
			);
		$data = $request->listingQuery($parameters, $this->type, array());
		return reset($data);
	}

	public function insertNew(){
		$request	= RequestManager::getInstance();
		$new_lns	= $this->getNewListings();
		echo "New <strong>".$this->type."</strong> Listings: ".count($new_lns)."<br>\n";
		if(count($new_lns) < 1){
			return TRUE;
		}
		$batches = array_chunk($new_lns, 100);
		$count = 1;
		foreach($batches as $batch){
			echo "Getting Batch $count Data: ";
			$listing_data = array();
			foreach($batch as $listing){
				$this->checkRunTime();
				$data = $this->getListing($listing['list_num']);
				if(is_array($data)){
					$listing_data[] = $data;
					echo ".";
				} else {
					var_dump($data);
					echo "x";
				}
			}
			$columns = array();
			//get the columns for the for first part of the sql statement.
			foreach($listing_data as $listing){
				foreach($listing as $column => $value){
					$column = $this->db_conn->escape_string($column);
					$columns[$column] = $column;
				}
			}
			$rows = array();
			foreach($listing_data as $listing){
				$row = array();
				foreach($columns as $column){
					$value = array_key_exists($column, $listing) ? $listing[$column] : '';
					$row[] = $this->db_conn->escape_string($value);
				}
				$rows[] = "('".implode("', '", $row)."')";
			}
			if(empty($rows)){
				error_log("insert missing rows!");
				continue;
			}
			if(empty($columns)){
				error_log("insert missing columns!");
				continue;
			}
			$rows = implode(",\n", $rows);
			$cols = "`".implode("`, `", $columns)."`";
			$sql = "INSERT IGNORE INTO `%s` (%s) VALUES\n%s";
			$sql = sprintf($sql, $this->data_tbl, $cols, $rows);
			$results = $this->db_conn->query($sql);
			if($results === FALSE){
				throw new Exception("Query failed: ".$this->db_conn->error);
			}
			echo " Batch Inserted!<br>\n";
			$count++;
		}
		echo "<strong>".$this->type."</strong> has finished inserting new listings to the system.<br>\n";
	}
	public function updateExisting(){
		$request	= RequestManager::getInstance();
		$update_lns	= $this->getUpdatedListings();
		echo "Updated <strong>".$this->type."</strong> Listings: ".count($update_lns)."<br>\n";
		if(count($update_lns) < 1){
			return TRUE;
		}
		foreach($update_lns as $listing){
			$this->checkRunTime();
			$ln = $this->db_conn->escape_string($listing['list_num']);
			$data = $this->getListing($ln);
			$rows = array();
			foreach($data as $column => $value){
				$column = $this->db_conn->escape_string($column);
				$value = $this->db_conn->escape_string($value);
				$rows[] = "`".$column."` = '".$value."'";
			}
			if(empty($rows)){
				error_log("update missing rows for $ln!");
				continue;
			}
			$sql = "UPDATE `%s` SET %s WHERE `ln` = '%s'";
			$sql = sprintf($sql, $this->data_tbl, implode(",\n", $rows), $ln);
			$results = $this->db_conn->query($sql);
			if($results === FALSE){
				throw new Exception("Query failed: ".$this->db_conn->error);
			}
			echo "Updated Listing: <strong>$ln</strong> !<br>\n";
		}
	}

	public function markDeleted(){
		$sql = "UPDATE `%s` LEFT JOIN `%s` on `ln` = `list_num` SET `st` = 'DEL' WHERE `list_num` IS NULL;";
		$sql = sprintf($sql, $this->data_tbl, $this->tmp_date_tbl);
		$results = $this->db_conn->query($sql);
		if($results === FALSE){
			throw new Exception("Query failed: ".$db_conn->error);
		}
		echo "Deleted <strong>".$this->type."</strong> Listings have been marked with the status DEL.<br>\n";
		return TRUE;
	}

	public function getActiveIds(){
		
	}

	private function getDataTblColumns(){
		$sql = 'SHOW COLUMNS FROM `'.$this->data_tbl.'`';
		$results = $this->db_conn->query($sql);
		if($results === FALSE){
			die("Query failed: ".$db_conn->error);
		}
		$columns = array();
		while($column = $results->fetch_array(MYSQLI_ASSOC)){
			$columns[] = $column['Field'];
		}
		return $columns;
	}

	private function checkRunTime(){
		if(self::$run_start === 0){
			self::$run_start = time();
		}
		$run_time = time()-self::$run_start;
		if($run_time > $this->max_run){
			echo "<br>\nThe Script has run out of time! Exiting</strong><br>\n";
			exit();
		}
		//echo "Run Time: ".floor(intval(($run_time/60))).":".($run_time%60)."<br>\n";
	}

	private function getStartTime(){
		$start_time = $this->getOldestActiveDate();
		$start_time = ($start_time === FALSE) ? $this->start_time : strtotime($start_time);
		if($start_time < $this->start_time){
			$start_time = $this->start_time;
		}
		return $start_time;
	}
}