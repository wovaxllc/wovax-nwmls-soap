<?php
ini_set('memory_limit','512M');
set_time_limit(0);
ignore_user_abort(true);

include_once('./login-info.php');
include_once('./settings-manager.php');
include_once('./xmlstr_to_array.php');//Using this simple library since it seems to work well

function get_agent_data($user, $pass){
	$client = new SoapClient('http://evernet.nwmls.com/evernetqueryservice/evernetquery.asmx?WSDL', array('trace' => 1));
	$XMLQuery = "<?xml version='1.0' encoding='utf-8' standalone='no' ?>";
    $XMLQuery .= "<EverNetQuerySpecification  xmlns='urn:www.nwmls.com/Schemas/General/EverNetQueryXML.xsd'>";
    $XMLQuery .= "<Message>";
    $XMLQuery .= "<Head>";
    $XMLQuery .= "<UserId>".$user."</UserId>";
    $XMLQuery .= "<Password>".$pass."</Password>";
    $XMLQuery .= "<SchemaName>StandardXML1_3</SchemaName>";
    $XMLQuery .= "</Head>";
    $XMLQuery .= "<Body>";
    $XMLQuery .= "<Query>";
    $XMLQuery .= "<MLS>NWMLS</MLS><ListingNumber></ListingNumber>";
    $XMLQuery .= "</Query>";
    $XMLQuery .= "<Filter></Filter>";
    $XMLQuery .= "</Body>";
    $XMLQuery .= "</Message>";
    $XMLQuery .= "</EverNetQuerySpecification >";
	$params = array ("v_strXmlQuery" => $XMLQuery);//This case senseitive such a time waster!!!
	$data = $client->RetrieveMemberData($params);
	$data = xmlstr_to_array($data->RetrieveMemberDataResult)['member'];
	return $data;
}

function get_office_data($user, $pass){
	$client = new SoapClient('http://evernet.nwmls.com/evernetqueryservice/evernetquery.asmx?WSDL', array('trace' => 1));
	$XMLQuery = "<?xml version='1.0' encoding='utf-8' standalone='no' ?>";
    $XMLQuery .= "<EverNetQuerySpecification  xmlns='urn:www.nwmls.com/Schemas/General/EverNetQueryXML.xsd'>";
    $XMLQuery .= "<Message>";
    $XMLQuery .= "<Head>";
    $XMLQuery .= "<UserId>".$user."</UserId>";
    $XMLQuery .= "<Password>".$pass."</Password>";
    $XMLQuery .= "<SchemaName>StandardXML1_3</SchemaName>";
    $XMLQuery .= "</Head>";
    $XMLQuery .= "<Body>";
    $XMLQuery .= "<Query>";
    $XMLQuery .= "<MLS>NWMLS</MLS><ListingNumber></ListingNumber>";
    $XMLQuery .= "</Query>";
    $XMLQuery .= "<Filter></Filter>";
    $XMLQuery .= "</Body>";
    $XMLQuery .= "</Message>";
    $XMLQuery .= "</EverNetQuerySpecification >";
	$params = array ("v_strXmlQuery" => $XMLQuery);//This case senseitive such a time waster!!!
	$data = $client->RetrieveOfficeData($params);
	$data = xmlstr_to_array($data->RetrieveOfficeDataResult)['office'];
	return $data;
}
echo "<h3>Starting Agent/Office Update Proccess</h3>\n";
$settings	= SettingsManger::getInstance();
$user		= $settings->get('NWMLS_Login', 'wovax');
$pass		= $settings->get('NWMLS_Password', 'yekE=Aze7u');
$agents		= get_agent_data($user, $pass);
$offices	= get_office_data($user, $pass);
$dbConn		= get_db_connection();

$sql = 'CREATE TABLE IF NOT EXISTS `NWMLS_Agents` (';
$sql .= '`agent_id` INT(10) NOT NULL PRIMARY KEY,';
$sql .= '`first_name` varchar(128) NOT NULL,';
$sql .= '`last_name` varchar(128) NOT NULL,';
$sql .= '`agent_office_id` INT(10) NOT NULL,';
$sql .= '`office_area_code` CHAR(8) NOT NULL,';
$sql .= '`office_phone` CHAR(8) NOT NULL,';
$sql .= '`office_ext` CHAR(8) NOT NULL';
$sql .= ') ENGINE=InnoDB DEFAULT CHARSET=utf8;';
$results = $dbConn->query($sql);
if($results === FALSE){
	die("Query failed: ".$dbConn->error);
}

$sql = 'CREATE TABLE IF NOT EXISTS `NWMLS_Offices` (';
$sql .= '`office_id` INT(10) NOT NULL PRIMARY KEY,';
$sql .= '`name` varchar(128) NOT NULL,';
$sql .= '`website` varchar(128) NOT NULL,';
$sql .= '`email` varchar(128) NOT NULL,';
$sql .= '`area_code` CHAR(8) NOT NULL,';
$sql .= '`phone` CHAR(8) NOT NULL,';
$sql .= '`address` varchar(128) NOT NULL,';
$sql .= '`city` varchar(128) NOT NULL,';
$sql .= '`state` varchar(128) NOT NULL,';
$sql .= '`zip` varchar(128) NOT NULL,';
$sql .= '`type` CHAR(10) NOT NULL';
$sql .= ') ENGINE=InnoDB DEFAULT CHARSET=utf8;';
$results = $dbConn->query($sql);
if($results === FALSE){
	die("Query failed: ".$dbConn->error);
}

foreach($agents as $agent){
	update_agent($dbConn, $agent);
}

foreach($offices as $office){
	update_office($dbConn, $office);
}

function update_agent($conn, $agent){
	$agent = mysqli_escape_array($conn, $agent);
	$sql = "SELECT * FROM `NWMLS_Agents` WHERE `agent_id` = ".$agent['MemberMLSID'];
	$ret = $conn->query($sql);
	if($ret === FALSE){
		die("Query failed: ".$conn->error);
	}
	if($tmp = $ret->fetch_array(MYSQLI_ASSOC)){
		$sql = "UPDATE `NWMLS_Agents` SET `agent_office_id` = ".$agent['OfficeMLSID'].", ";
		$sql .= "`first_name` = '".$agent['FirstName']."', ";
		$sql .= "`last_name` = '".$agent['LastName']."', ";
		$sql .= "`office_area_code` = '".$agent["OfficeAreaCode"]."', ";
		$sql .= "`office_phone` = '".$agent["OfficePhone"]."', ";
		$sql .= "`office_ext` = '".$agent["OfficePhoneExtension"]."' ";
		$sql .= "WHERE `agent_id` = ".$agent['MemberMLSID'].";";
	} else {
		$sql =  "INSERT INTO `NWMLS_Agents` (`agent_id`, `agent_office_id`, `first_name`, `last_name`, `office_area_code`, `office_phone`, `office_ext`) VALUES ";
		$sql .= "(".$agent['MemberMLSID'].", ".$agent['OfficeMLSID'].", '".$agent['FirstName']."', '".$agent['LastName']."', '";
		$sql .= $agent["OfficeAreaCode"]."', '".$agent["OfficePhone"]."', '".$agent["OfficePhoneExtension"]."');";
	}
	echo $sql."<br>\n";
	$ret = $conn->query($sql);
	if($ret === FALSE){
		die("Query failed: ".$conn->error);
	}
	return $ret;
}


function update_office($conn, $office){
	$office = mysqli_escape_array($conn, $office);
	$sql = "SELECT * FROM `NWMLS_Offices` WHERE `office_id` = ".$office["OfficeMLSID"];
	$ret = $conn->query($sql);
	if($ret === FALSE){
		die("Query failed: ".$conn->error);
	}
	if($tmp = $ret->fetch_array(MYSQLI_ASSOC)){
		$sql = "UPDATE `NWMLS_Offices` SET ";
		$sql .= "`name` = '".$office["OfficeName"]."', ";
		$sql .= "`address` = '".$office["StreetAddress"]."', ";
		$sql .= "`city` = '".$office["StreetCity"]."', ";
		$sql .= "`state` = '".$office["StreetState"]."', ";
		$sql .= "`zip` = '".$office["StreetZipCode"]."', ";
		$sql .= "`area_code` = '".$office["OfficeAreaCode"]."', ";
		$sql .= "`phone` = '".$office["OfficePhone"]."', ";
		$sql .= "`email` = '".$office["EMailAddress"]."', ";
		$sql .= "`website` = '".$office["WebPageAddress"]."', ";
		$sql .= "`type` = '".$office["OfficeType"]."' ";
		$sql .= "WHERE `office_id` = ".$office["OfficeMLSID"].";";
	} else{
		$sql = "INSERT INTO `NWMLS_Offices` ";
		$sql .= "(`office_id`, ";
		$sql .= "`name`, ";
		$sql .= "`address`, ";
		$sql .= "`city`, ";
		$sql .= "`state`, ";
		$sql .= "`zip`, ";
		$sql .= "`area_code`, ";
		$sql .= "`phone`, ";
		$sql .= "`email`, ";
		$sql .= "`website`, ";
		$sql .= "`type`) VALUES ";
		$sql .= "(".$office["OfficeMLSID"].", ";
		$sql .= "'".$office["OfficeName"]."', ";
		$sql .= "'".$office["StreetAddress"]."', ";
		$sql .= "'".$office["StreetCity"]."', ";
		$sql .= "'".$office["StreetState"]."', ";
		$sql .= "'".$office["StreetZipCode"]."', ";
		$sql .= "'".$office["OfficeAreaCode"]."', ";
		$sql .= "'".$office["OfficePhone"]."', ";
		$sql .= "'".$office["EMailAddress"]."', ";
		$sql .= "'".$office["WebPageAddress"]."', ";
		$sql .= "'".$office["OfficeType"]."');";
	}
	echo $sql."<br>\n";
	$ret = $conn->query($sql);
	if($ret === FALSE){
		die("Query failed: ".$conn->error);
	}
	return $ret;
}