<?php
define('DB_Login', 'wovaxio_rets');
define('DB_Password', 'bdJNSVd5efU956');
define('DB_Server', 'localhost');
define('DB_Name', 'wovaxio_NWMLS');

global $__wovaxio_db;

function get_db_connection(){
	global $__wovaxio_db;
	if($__wovaxio_db Instanceof mysqli){
		return $__wovaxio_db;
	}
	$conn = new mysqli(DB_Server, DB_Login, DB_Password);
	$conn->set_charset('utf8mb4');
	// Check connection
	if (!$conn) {
		die("Connection failed: " . mysqli_connect_error());
	}
	//echo "Connected successfully to the database.<br>\n";
	if($conn->select_db(DB_Name) === FALSE){
		die("DB switch failed: ".$conn->error);
	}
	$__wovaxio_db = $conn;
	return $__wovaxio_db;
}

function close_db(){
	global $__wovaxio_db;
	if($__wovaxio_db Instanceof mysqli){
		$__wovaxio_db->close();
		$__wovaxio_db=NULL;
	}
}

function mysqli_escape_array($dbConn, $array){
	foreach($array as $key => $val){
		if(!is_string($val)){
			$array[$key] = '';
		} else {
			$array[$key] = $dbConn->escape_string($val);
		}
	}
	return $array;
}