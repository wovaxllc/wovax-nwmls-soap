<?php
include_once('./login-info.php');
include_once('./request-manager.php');
include_once('./settings-manager.php');

Class PictureUpdate {
	private $db_conn = NULL;
	private $status_tbl = 'NWMLS_Pic_Statuses';
	private $type = '';
	public function __construct($type){
		$this->type = $type;
		$this->db_conn = get_db_connection();
	}

	public function createPicStatusTable(){
		$sql = "CREATE TABLE `NWMLS_Status` (
			`listing_number` int(10) NOT NULL,
			`photo_update` datetime NOT NULL,
			`
			`photo_status` int(10) DEFAULT '0',
			`update_checked` datetime NOT NULL,
			`hash` char(40) NOT NULL,
			PRIMARY KEY (`listing_number`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
		$sql = "CREATE TABLE IF NOT EXISTS `%s` (
		`'.$this->ln_col.'` INT(10) UNSIGNED NOT NULL PRIMARY KEY";
		$sql .= '`'.$this->photo_chk_col.'` DATETIME NOT NULL,';
		$sql .= '`'.$this->photo_stat_col.'` INT(10) DEFAULT 0,';
		$sql .= '`'.$this->update_col.'` DATETIME NOT NULL,';
		$sql .= '`'.$this->hash_col.'` CHAR(40) NOT NULL';
		$sql .= ') ENGINE=InnoDB DEFAULT CHARSET=utf8;';
		$results = $this->query($sql);
	}
}