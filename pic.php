<?php
include_once('./settings-manager.php');
include_once('./xmlstr_to_array.php');//Using this simple library since it seems to work well
//this is to prevent timing attacks
//compare each character before return.
function string_chk($known_str, $user_str){
	//Just exit if the strings are not equal in length.
	$strlen = strlen($known_str);
	if($strlen != strlen($user_str)){
		return false;
	}
	$ret = true;
	for($i = 0; $i <= $strlen; $i++) {
		$known_char = substr($known_str, $i, 1);
		$user_char = substr($user_str, $i, 1);
		if(!($known_char === $user_char)){
			$ret = false;
		}
	}
	return $ret;
}


function get_photo($user, $pass, $file_name){
	//NWMLS TEST QUERY PAGE for images: http://images.idx.nwmls.com/imageservicetest/
	$client = new SoapClient('http://images.idx.nwmls.com/imageservice/imagequery.asmx?WSDL', array('trace' => 1));
	$XMLQuery = '<?xml version="1.0"?><ImageQuery xmlns="NWMLS:EverNet:ImageQuery:1.0">';
	$XMLQuery .= '<Auth>';
	$XMLQuery .= '<UserId>'.$user.'</UserId>';
	$XMLQuery .= '<Password>'.$pass.'</Password>';
	$XMLQuery .= '</Auth>';
	$XMLQuery .= '<Query>';
	$XMLQuery .= '<ByName>'.$file_name.'</ByName>';
	$XMLQuery .= '</Query>';
	$XMLQuery .= '<Results>';
	$XMLQuery .= '<Schema>NWMLS:EverNet:ImageData:1.0</Schema>';
	$XMLQuery .= '</Results>';
	$XMLQuery .= '</ImageQuery>';
	$params = array ("query" => $XMLQuery);//This case senseitive such a time waster!!!
	$data = $client->RetrieveImages($params);
	$data = xmlstr_to_array($data->RetrieveImagesResult)['Images']['Image']['BLOB'];
	return base64_decode($data);;
}

$settings	= SettingsManger::getInstance();
$user		= $settings->get('NWMLS_Login', 'wovax');
$pass		= $settings->get('NWMLS_Password', 'yekE=Aze7u');
$photo_key  = $settings->get('photo_key', 'W5Jg2aHAZ28zscZX4Xzcxh');
$request = $_SERVER[REQUEST_URI];
$file = basename(__FILE__);
$info = explode('/',substr($request, strpos($request, $file)+strlen($file)+1));//the plus removes the leading slash
$filler = $info[0];
$user_hash = $info[1];
$file = $info[2];
$extension = explode('.', $file)[1];
$chk_hash = hash_hmac('sha1', $filler.'|'.$file, $photo_key);
if(!string_chk($chk_hash, $user_hash)){
	header('HTTP/1.1 400 Bad Request', true, 400);
	?>
	<html>
		<head>
			<title>Bad Hash</title>
		</head>
		<body>
			<center>
				<h3>Bad Hash</h3>
				<p>I am not sure how you got here, but the url has an invalid hash for the requested resource.</p>
			</center>
		</body>
		 <footer>
			<center>
				<p><a href="http://www.wovax.com">WOVAX LCC</a></p>
				<p><?php echo date("Y"); ?></p>
			</center>
		</footer> 
	</html>
	<?php 
	exit();
}
switch ($extension){
	case 'jpg':
	case 'jpeg':
		header("Content-Type: image/jpeg");
		break;
	case 'png':
		header('Content-Type: image/png');
		break;
	case 'gif':
		header('Content-Type: image/gif');
		break;
	default:
		header('HTTP/1.1 400 Bad Request', true, 400);
		?>
		<html>
			<head>
				<title>Bad Extension</title>
			</head>
			<body>
				<center>
					<h3>Bad Extension</h3>
					<p>The requested resource has an invalid file extension.</p>
				</center>
			</body>
			<footer>
				<center>
					<p><a href="http://www.wovax.com">WOVAX LCC</a></p>
					<p><?php echo date("Y"); ?></p>
				</center>
			</footer> 
		</html>
		<?php
		error_log('Bad File Extension: '.$request);
		exit();
		break;
}
echo get_photo($user, $pass, $file);