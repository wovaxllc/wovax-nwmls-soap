<?php
include_once('./login-info.php');

Class StatusManager{
	private static $instance = NULL;
	private $table			= 'NWMLS_Status';
	private $ln_col			= 'listing_number';
	private $hash_col		= 'hash';
	private $update_col 	= 'update_checked';
	private $photo_chk_col	= 'photo_checked';
	private $photo_stat_col	= 'photo_status';

	private function buildTable(){
		$db_conn = get_db_connection();
		$sql = 'CREATE TABLE IF NOT EXISTS `'.$this->table.'` (';
		$sql .= '`'.$this->ln_col.'` INT(10) NOT NULL PRIMARY KEY,';
		$sql .= '`'.$this->photo_chk_col.'` DATETIME NOT NULL,';
		$sql .= '`'.$this->photo_stat_col.'` INT(10) DEFAULT 0,';
		$sql .= '`'.$this->update_col.'` DATETIME NOT NULL,';
		$sql .= '`'.$this->hash_col.'` CHAR(40) NOT NULL';
		$sql .= ') ENGINE=InnoDB DEFAULT CHARSET=utf8;';
		$results = $this->query($sql);
		return true;
	}
	//gets the one instance of this class.
	public static function getInstance() {
		if (is_null(self::$instance)) {
			self::$instance = new self;
			self::$instance->buildTable();
		}
		return self::$instance;
	}
	
	public function statusExists($list_num){
		$list_num = $this->validateInt($list_num);
		$sql = 'SELECT count(*) as `count` FROM `'.$this->table.'` WHERE `'.$this->ln_col.'` = '.$list_num.';';
		$results = $this->query($sql);
		return (intval($results->fetch_array(MYSQLI_ASSOC)['count']) > 0);
	}
	
	public function getHash($list_num){
		$list_num = $this->validateInt($list_num);
		if(!$this->statusExists($list_num)){
			return '';
		}
		$sql = 'SELECT `'.$this->hash_col.'` FROM `'.$this->table.'` WHERE `'.$this->ln_col.'` = '.$list_num.';';
		$results = $this->query($sql);
		return $results->fetch_array(MYSQLI_ASSOC)[$this->hash_col];
	}
	
	function updateHash($list_num, $hash){
		$list_num = $this->validateInt($list_num);
		if(!is_string($hash) OR (strlen($hash) !== 40) OR !ctype_xdigit($hash)){
			die("Invalid SHA1 hash: ".strval($hash));
		}
		$sql = '';
		if($this->statusExists($list_num)){
			$sql .= "UPDATE `".$this->table."` SET `".$this->hash_col."` = '".$hash."', `".$this->update_col."` = '".date("Y-m-d H:i:s")."' ";
			$sql .= 'WHERE `'.$this->ln_col.'` = '.$list_num.';';
		} else {
			$sql .= 'INSERT INTO `'.$this->table.'` (`'.$this->ln_col.'`, `'.$this->hash_col.'`, `'.$this->update_col.'`) VALUES ';
			$sql .= '('.$list_num.", '".$hash."', '".date("Y-m-d H:i:s")."');";
		}
		$this->query($sql);
		return true;
	}
	
	function insertHashList($hashes){
	    $count = count($hashes);
	    $sql = 'INSERT INTO `'.$this->table.'` (`'.$this->ln_col.'`, `'.$this->hash_col.'`, `'.$this->update_col.'`) VALUES ';
	    for($i = 0; $i < $count; $i++){
	        $hash =  $hashes[$i]['hash'];
	        $ln = $this->validateInt($hashes[$i]['ln']); 
	        if((strlen($hash) !== 40) OR !ctype_xdigit($hash)){
			    die("Invalid SHA1 hash: ".strval($hash));
		    }
	        $sql .= '('.$ln.", '".$hash."', '".date("Y-m-d H:i:s")."')";
	        if($i != $count-1){
	            $sql .= ', ';
	        }
	    }
	    $this->query($sql);
	    return true;
    }
	
	function getListingsByPhotoStatus($status){
		$status = $this->validateInt($status);
		if($status > 3 OR $status < 0){
			die("Invalid Status: ".$status);
		}
		$sql = "SELECT `".$this->ln_col."` FROM `".$this->table."` WHERE `".$this->photo_stat_col."` = ".$status.";";
		$results = $this->query($sql);
		$lns = array();
		while($ln = $results->fetch_array(MYSQLI_ASSOC)){
			$lns[] = $this->validateInt($ln[$this->ln_col]);
		}
		shuffle($lns);
		return $lns;
	}
	
	function updatePhotoStatusMulti($listings){
		foreach($listings as $listing){
			$this->updatePhotoStatus($listing['ln'], $listing['status']);
		}
		return true;
	}
	
	function updatePhotoStatus($list_num, $status){
		$list_num = $this->validateInt($list_num);
		$status = $this->validateInt($status);
		if($status > 3 OR $status < 0){
			die("Invalid Status: ".$status);
		}
		$sql = '';
		if($this->statusExists($list_num)){
			$sql .= "UPDATE `".$this->table."` SET `".$this->photo_stat_col."` = ".$status.", `".$this->photo_chk_col."` = '".date("Y-m-d H:i:s")."' ";
			$sql .= 'WHERE `'.$this->ln_col.'` = '.$list_num.';';
		} else {
			$sql .= 'INSERT INTO `'.$this->table.'` (`'.$this->ln_col.'`, `'.$this->photo_stat_col.'`, `'.$this->photo_chk_col.'`) VALUES ';
			$sql .= '('.$list_num.", '".$status."', '".date("Y-m-d H:i:s")."');";
		}
		$this->query($sql);
		return true;
	}
	
	private function query($sql){
		$db_conn = get_db_connection();
		$results = $db_conn->query($sql);
		if($results === FALSE){
			die("Query failed: ".$db_conn->error);
		}
		return $results;
	}
	
	private function validateInt($int){
		if(is_integer($int)){
			return $int;
		}
		if(is_string($int) AND ctype_digit($int)){
			return intval($int);
		}
		die("This is not an int: ".strval($list_num));
	}
}


