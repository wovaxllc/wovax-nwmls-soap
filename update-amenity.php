<?php
ini_set('memory_limit','256M');
set_time_limit(0);
ignore_user_abort(true);

include_once('./login-info.php');
include_once('./settings-manager.php');
include_once('./xmlstr_to_array.php');//Using this simple library since it seems to work well

function get_data_map($login, $password, $type){
	$client = new SoapClient('http://evernet.nwmls.com/evernetqueryservice/evernetquery.asmx?WSDL', array('trace' => 1));
	$XMLQuery = "<?xml version='1.0' encoding='utf-8' standalone='no' ?>";
    $XMLQuery .= "<EverNetQuerySpecification  xmlns='urn:www.nwmls.com/Schemas/General/EverNetQueryXML.xsd'>";
    $XMLQuery .= "<Message>";
    $XMLQuery .= "<Head>";
    $XMLQuery .= "<UserId>".$login."</UserId>";
    $XMLQuery .= "<Password>".$password."</Password>";
    $XMLQuery .= "<SchemaName>StandardXML1_3</SchemaName>";
    $XMLQuery .= "</Head>";
    $XMLQuery .= "<Body>";
    $XMLQuery .= "<Query>";
    $XMLQuery .= "<MLS>NWMLS</MLS><ListingNumber></ListingNumber><PropertyType>".$type."</PropertyType>";
    $XMLQuery .= "</Query>";
    $XMLQuery .= "<Filter></Filter>";
    $XMLQuery .= "</Body>";
    $XMLQuery .= "</Message>";
    $XMLQuery .= "</EverNetQuerySpecification >";
	$params = array ("v_strXmlQuery" => $XMLQuery);//This case senseitive such a time waster!!!
	$data = $client->RetrieveAmenityData($params);
	$data = xmlstr_to_array($data->RetrieveAmenityDataResult)['Amenity'];
	return $data;
}


echo "<h3>Starting Amenity Update Proccess</h3>\n";
$settings	= SettingsManger::getInstance();
$prop_types	= $settings->get('properties_to_update', array(
	'BUSO',
	'RESI',
	'MANU',
	'FARM',
	'COMI',
	'VACL',
	'MULT',
	'COND'
));
$user		= $settings->get('NWMLS_Login', 'wovax');
$pwd		= $settings->get('NWMLS_Password', 'yekE=Aze7u');
$data		= array();

foreach($prop_types as $type){
	$data = array_merge($data, get_data_map($user, $pwd, $type));
}

foreach($data as $data_map){
	$code = $data_map['Values']['Code'];
	$desc = $data_map['Values']['Description'];
	$column = $data_map['Code'];
	switch ($column) {
		case 'STY':
			check_tbl($column);
			$desc = preg_replace('/^([0-9][0-9]? - )/', '', $desc);
			echo 'Adding to '.$column.' | '.$code.' | ';
			echo $desc."<br>\n";
			update_col_map(get_db_connection(), $column, $code, $desc);
			break;
		default:
			continue;
			break;
	}
}

global $checks;
function check_tbl($col){
	global $checks;
	if(empty($check)){
		$checks = array();
	}
	if(array_key_exists($col, $checks)){
		return true;
	}
	$sql = 'CREATE TABLE IF NOT EXISTS `NWMLS_'.$col.'` (';
	$sql .= '`id` INT(10) NOT NULL PRIMARY KEY AUTO_INCREMENT,';
	$sql .= '`'.$col.'_code` varchar(16) NOT NULL,';
	$sql .= '`'.$col.'_value` varchar(128) NOT NULL,';
	$sql .= 'UNIQUE KEY `'.$col.'_key` (`'.$col.'_code`)';
	$sql .= ') ENGINE=InnoDB DEFAULT CHARSET=utf8;';
	$results = get_db_connection()->query($sql);
	if($results === FALSE){
		die("Query failed: ".get_db_connection()->error);
	}
	$checks[$col] = true;
	return true;
}

function update_col_map($conn, $column, $code, $value){
	$sql = "SELECT * FROM `NWMLS_".$column."` WHERE `".$column."_code` = '".$code."';";
	$ret = $conn->query($sql);
	if($tmp = $ret->fetch_array(MYSQLI_ASSOC)){
		$sql = "UPDATE `NWMLS_".$column."` SET `".$column."_value` = '".$value."' WHERE `".$column."_code` = '".$code."';";
	} else {
		$sql =  "INSERT INTO `NWMLS_".$column."` (`".$column."_code`, `".$column."_value`) VALUES ";
		$sql .= "('".$code."', '".$value."');";
	}
	//echo $sql."<br>\n";
	return $conn->query($sql);
}