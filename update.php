<?php
ini_set('memory_limit','2048M');
ini_set('display_errors', '1');
include_once('./login-info.php');
include_once('./update-tables.php');
include_once('./settings-manager.php');

echo "<h3>Starting Update Proccess</h3>\n";
$settings = SettingsManger::getInstance();

$prop_types		= $settings->get('properties_to_update', array(
	'BUSO',
	'RESI',
	'MANU',
	'FARM',
	'COMI',
	'VACL',
	'MULT',
	'COND'
));
$step_size		= $settings->get('day_step_size', 4);
$restart_date	= $settings->get('restart_date', strtotime('January 1 2014'));
$start_date		= $settings->get('start_date', strtotime('January 1 2014'));
$user			= $settings->get('NWMLS_Login', 'wovax');
$pwd			= $settings->get('NWMLS_Password', 'yekE=Aze7u');
if($start_date >= time()){
	$start_date = $restart_date;
}
$new_date		= $start_date+(86400*$step_size);
$settings->update('start_date', $new_date);
//update the data table columns
update_data_tables($prop_types, $user, $pwd);
update_data_in_tables($prop_types, $step_size, $restart_date, $start_date, $user, $pwd);
echo "Done!<br>\n";
$refresh = array_key_exists('refresh', $_REQUEST) ? $_REQUEST['refresh'] : '0';
if($refresh !== '0'){
	echo '<meta http-equiv="refresh" content="1;url='.basename(__FILE__, '').'?refresh=1">';
}
close_db();