<?php
include_once('./listings.php');
include_once('./status-manager.php');
include_once('./login-info.php');

function update_data_in_tables($prop_types, $day_step, $restart_date, $start_date, $user, $password){
	$db_conn = get_db_connection();
	$status = StatusManager::getInstance();
	$begin_date	= date("Y-m-d", $start_date)."T00:00:00";
	$end_date	= date("Y-m-d", $start_date+(86400*$day_step))."T00:00:00";
	echo "Getting properties for date range: <b>".$begin_date."</b> - <b>".$end_date."</b><br>\n";
	foreach($prop_types as $type){
		$listings = get_listings_by_date($user, $password, $type, $begin_date, $end_date);
		$count = count($listings);
		$update = array();
		$insert = array();
		$list_nums = array();
		$table = $type."_Data";
		$columns = get_columns($table);
		foreach($columns as $index => $col){
			if($col === 'id'){
				unset($columns[$index]);
				break;
			}
		}
		echo "<b>".$type."</b> count: <b>".$count."</b>";
		echo " (".convert(memory_get_usage(true)).")<br>\n";
		if($count < 1){
			continue;
		}
		foreach($listings as $listing){
			$listing = array_give_keys($listing, $columns);
			$escaped_listing = array();
			foreach($listing as $col => $val){
				$escaped_listing[$col] = $db_conn->escape_string($val);
			}
			$listing = $escaped_listing;
			$list_nums[] = $listing['ln'];
			if($status->statusExists($listing['ln'])){
				$update[] = $listing;
			} else {
				$insert[] = $listing;
			}
		}
		$existing = find_existing($table, $list_nums);
		foreach($insert as $index => $listing){
			if(in_array($listin['ln'], $existing)){
				$update[] = $listing;
				unset($insert[$index]);
			}
		}
		$insert = array_values($insert);
		insert_listings($table, $insert);
		update_listings($table, $update);
	}
	return true;
}

function insert_listings($table, $listings){
	if(empty($listings) OR !is_array($listings)){
		return;
	}
	$status = StatusManager::getInstance();
    $db_conn = get_db_connection();
	$insert_parts = array();
	$insert_hashes = array();
	foreach($listings as $listing){
		$hash = sha1_array($listing);
		$insert_parts[] = "('".implode("', '", $listing)."')";
		$insert_hashes[] = array('ln' => $listing['ln'], 'hash' => $hash);
	}
	if(!empty($insert_parts)){
		$sql = "INSERT INTO `".$table."` (`".implode("`, `", array_keys($listings[0]))."`) VALUES ";
		$sql .= implode(", ", $insert_parts).";";
		$results = $db_conn->query($sql);
		if($results === FALSE){
			echo $sql;
			die("Query failed: ".$db_conn->error);
		}
		$status->insertHashList($insert_hashes);
		echo "<b>".$table."</b> inserted: <b>".count($listings)."</b><br>\n";
	}
}

function update_listings($table, $listings){
	if(empty($listings) OR !is_array($listings)){
		return;
	}
	$status = StatusManager::getInstance();
    $db_conn = get_db_connection();
	foreach($listings as $listing){
		$sql_str = '';
		$hash = sha1_array($listing);
		$ln = $listing['ln'];
		unset($listing['ln']);
		if($hash === $status->getHash($ln)){
			continue;
		}
		$sql_part = implode_key_and_value(", ", " = ", $listing, "'", "`");
		$sql_str .= 'UPDATE `'.$table.'` SET '.$sql_part.' WHERE `ln` = '.$ln.";";
		$results = $db_conn->query($sql_str);
		if($results === FALSE){
			die("Query failed: ".$db_conn->error);
		}
		$status->updateHash($ln, $hash);
		echo 'Updated list number: <b>'.$ln."</b></br>\n";
	}
}

function find_existing($table, $list_numbers){
	if(empty($list_numbers) OR !is_array($list_numbers)){
		return array();
	}
	$db_conn = get_db_connection();
	$sql = "SELECT `ln` FROM `".$table."` WHERE `ln` IN (".implode(', ', $list_numbers).");";
	$results = $db_conn->query($sql);
	if($results === FALSE){
		die("Query failed: ".$db_conn->error);
	}
	$exists = array();
	while($ln = $results->fetch_array(MYSQLI_ASSOC)){
		$exists[] = $ln['COLUMN_NAME'];
	}
	return $exists;
}

function implode_key_and_value($val_glue, $key_glue, $array, $val_wrap = '', $key_wrap = '', $key_first = TRUE){
	$count = count($array);
	$i = 0;
	$ret = '';
	foreach($array as $key => $val){
		$key = $key_wrap.$key.$key_wrap;
		$val = $val_wrap.$val.$val_wrap;
		$ret .= $key_first ? $key.$key_glue.$val : $val.$key_glue.$key;
		if($i !== ($count-1)){
			$ret .= $val_glue;
		}
		$i++;
	}
	return $ret;
}

function sha1_array($array){
	$str = implode('|', $array);
	//echo $str."<br>\n";
	return sha1($str);
}

function get_columns($table){
	$db_conn = get_db_connection();
	$sql = 'SHOW COLUMNS FROM `'.$table.'`';
	$results = $db_conn->query($sql);
	if($results === FALSE){
		die("Query failed: ".$db_conn->error);
	}
	$columns = array();
	while($column = $results->fetch_array(MYSQLI_ASSOC)){
		$columns[] = $column['Field'];
	}
	return $columns;
}

function array_give_keys($array, $keys){
	$master = array();
	foreach($keys as $key){
		$master[$key] = '';
	}
	$new_array = $master;
	foreach($new_array as $key => $val){
		if(array_key_exists($key, $array)){
			$new_array[$key] = $array[$key];
		}
	}
	return $new_array;
}

function convert($size){
    $unit=array('b','kb','mb','gb','tb','pb');
    return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
}